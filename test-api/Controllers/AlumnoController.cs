﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using test_api.Models;

namespace test_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnoController : ControllerBase
    {
        private readonly testapiContext _context;

        public AlumnoController(testapiContext context)
        {
            _context = context;
        }

        // GET: Alumno
        [HttpGet]
        [Route("Index")]
        public IActionResult Index()
        {
            List<Alumno> list = new List<Alumno>();
            try
            {
                list = this._context.Alumnos
                    .Include(o => o.tipoDocumento)
                    .Where(o => o.DeletedAt == null)
                    .ToList();
                return StatusCode(StatusCodes.Status200OK, list);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = ex.Message, response = list });
            }
        }

        // GET: Alumno/Details/5
        [HttpGet]
        [Route("Details/{id:int}")]
        public IActionResult Details(int? id)
        {
            if (id == null || _context.Alumnos == null)
            {
                return NotFound();
            }

            var alumno = _context.Alumnos
                .Where(o => o.DeletedAt == null)
                .FirstOrDefault(m => m.IdAlumno == id);
            if (alumno == null)
            {
                return NotFound();
            }
            return StatusCode(StatusCodes.Status200OK, alumno);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create([Bind("Nombres,Apellidos,Email,IdTipoDocumento,NumeroDocumento,FecNacimiento,Sexo,Telefono,Nacionalidad,ObjectId")] Alumno alumno)
        {
            if (!ModelState.IsValid)
            {
                string message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                return BadRequest(message);
            }

            _context.Alumnos.Add(alumno);
            _context.SaveChanges();
            return StatusCode(StatusCodes.Status201Created, alumno);
        }

        [HttpPost]
        [Route("Update/{id:int}")]
        public IActionResult Update(int id, [Bind("Nombres,Apellidos,Email,IdTipoDocumento,NumeroDocumento,FecNacimiento,Sexo,Telefono,Nacionalidad,ObjectId")] Alumno alumno)
        {
            if (id != alumno.IdAlumno)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                string message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                return BadRequest(message);
            }

            try
            {
                _context.Update(alumno);
                _context.SaveChanges();
                return StatusCode(StatusCodes.Status204NoContent);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlumnoExists(alumno.IdAlumno))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // POST: Alumno/Delete/5
        [HttpDelete]
        [Route("Delete/{id:int}")]
        public IActionResult Delete(int id)
        {
            if (_context.Alumnos == null)
            {
                return Problem("Entity set 'testapiContext.Alumnos'  is null.");
            }
            var alumno = _context.Alumnos.Find(id);
            if (alumno != null)
            {
                _context.Alumnos.Remove(alumno);
            }
            
            _context.SaveChanges();
            return StatusCode(StatusCodes.Status204NoContent);
        }

        private bool AlumnoExists(int id)
        {
          return (_context.Alumnos?.Any(e => e.IdAlumno == id)).GetValueOrDefault();
        }
    }
}
