﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using test_api.Models;

namespace test_api.Controllers
{
    [Route("api/[controller]")]
    public class TipoDocumentoController : ControllerBase
    {
        private readonly testapiContext _context;
        public TipoDocumentoController(testapiContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("Index")]
        public IActionResult Index()
        {
            List<TipoDocumento> list = new List<TipoDocumento>();
            try
            {
                list = this._context.TipoDocumentos.ToList();
                return StatusCode(StatusCodes.Status200OK, list);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = ex.Message, response = list });
            }
        }
    }
}
