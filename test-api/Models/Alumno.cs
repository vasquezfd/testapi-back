﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Text.Json.Serialization;

namespace test_api.Models
{
    public partial class Alumno : ISoftDelete, IUseTimeStamps
    {
        public int IdAlumno { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Nombres debe ser menor de 50 caracteres")]
        public string? Nombres { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Apellidos debe ser menor de 100 caracteres")]
        public string? Apellidos { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Email debe ser menor de 50 caracteres")]
        [EmailAddress]
        public string? Email { get; set; }
        [Required]
        [CustomValidation(typeof(Alumno), "ValidateTipoDocumento")]
        public int IdTipoDocumento { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Numero_documento debe tener un máximo de 20 caracteres")]
        public string? NumeroDocumento { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateOnly? FecNacimiento { get; set; }
        [Required]
        public bool? Sexo { get; set; }
        [StringLength(50, ErrorMessage = "Telefono debe tener un m áximo de 50 caracteres")]
        public string? Telefono { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Telefono debe tener un m áximo de 50 caracteres")]
        public string? Nacionalidad { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }
        [StringLength(255, ErrorMessage = "ObjectId debe tener un m áximo de 50 caracteres")]
        public string? ObjectId { get; set; }

        public virtual TipoDocumento? tipoDocumento { get; set; }

        public static ValidationResult ValidateTipoDocumento(string param, ValidationContext validationContext)
        {
            testapiContext? _context = validationContext.GetService(typeof(testapiContext)) as testapiContext;
            var result = _context.TipoDocumentos.Where(t => param.Contains(t.IdTipoDoc.ToString()));

            if (result.Count() <= 0)
            {
                return new ValidationResult("Tipo Documento no existe");
            }
            return ValidationResult.Success;
        }
    }
}
