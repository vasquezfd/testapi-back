﻿namespace test_api.Models
{
    public interface IUseTimeStamps
    {

        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public void Undo()
        {
            CreatedAt = null;
            UpdatedAt = null;
        }
    }
}
