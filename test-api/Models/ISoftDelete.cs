﻿namespace test_api.Models
{
    public interface ISoftDelete
    {

        public DateTime? DeletedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public void Undo()
        {
            CreatedAt = null;
            UpdatedAt = null;
            DeletedAt = null;
        }
    }
}
