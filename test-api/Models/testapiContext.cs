﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace test_api.Models
{
    public partial class testapiContext : DbContext
    {
        public testapiContext()
        {
        }

        public testapiContext(DbContextOptions<testapiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Alumno> Alumnos { get; set; } = null!;
        public virtual DbSet<TipoDocumento> TipoDocumentos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alumno>(entity =>
            {
                entity.HasKey(e => e.IdAlumno)
                    .HasName("alumno_pkey");

                entity.ToTable("alumno");

                entity.Property(e => e.IdAlumno).HasColumnName("id_alumno");

                entity.Property(e => e.Apellidos)
                    .HasMaxLength(100)
                    .HasColumnName("apellidos");

                entity.Property(e => e.CreatedAt)
                    .HasPrecision(0)
                    .HasColumnName("created_at");

                entity.Property(e => e.DeletedAt)
                    .HasPrecision(0)
                    .HasColumnName("deleted_at");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.FecNacimiento).HasColumnName("fec_nacimiento");

                entity.Property(e => e.IdTipoDocumento).HasColumnName("id_tipo_documento");

                entity.Property(e => e.Nacionalidad)
                    .HasMaxLength(50)
                    .HasColumnName("nacionalidad");

                entity.Property(e => e.Nombres)
                    .HasMaxLength(50)
                    .HasColumnName("nombres");

                entity.Property(e => e.NumeroDocumento)
                    .HasMaxLength(20)
                    .HasColumnName("numero_documento");

                entity.Property(e => e.ObjectId)
                    .HasMaxLength(255)
                    .HasColumnName("object_id");

                entity.Property(e => e.Sexo).HasColumnName("sexo");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .HasColumnName("telefono");

                entity.Property(e => e.UpdatedAt)
                    .HasPrecision(0)
                    .HasColumnName("updated_at");

                entity.HasOne(d => d.tipoDocumento)
                    .WithMany(p => p.Alumnos)
                    .HasForeignKey(d => d.IdTipoDocumento)
                    .HasConstraintName("alumno_tipo_doc_fk");
            });

            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.HasKey(e => e.IdTipoDoc)
                    .HasName("tipo_documento_pkey");

                entity.ToTable("tipo_documento");

                entity.Property(e => e.IdTipoDoc).HasColumnName("id_tipo_doc");

                entity.Property(e => e.TipoDoc)
                    .HasMaxLength(50)
                    .HasColumnName("tipo_doc");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
