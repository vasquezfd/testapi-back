﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace test_api.Models
{
    public partial class TipoDocumento
    {
        public TipoDocumento()
        {
            Alumnos = new HashSet<Alumno>();
        }

        public int IdTipoDoc { get; set; }
        public string TipoDoc { get; set; } = null!;

        [JsonIgnore]
        public virtual ICollection<Alumno> Alumnos { get; set; }
    }
}
