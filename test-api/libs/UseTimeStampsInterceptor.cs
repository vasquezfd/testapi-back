﻿namespace test_api.libs
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Diagnostics;
    using test_api.Models;

    public class UseTimeStampsInterceptor : SaveChangesInterceptor
    {
        public override InterceptionResult<int> SavingChanges(
            DbContextEventData eventData,
            InterceptionResult<int> result
        )
        {
            if (eventData.Context is null) return result;

            foreach (var entry in eventData.Context.ChangeTracker.Entries())
            {
                if (entry is not { State: EntityState.Added, Entity: ISoftDelete added }) continue;
                added.CreatedAt = DateTime.UtcNow;
                added.UpdatedAt = DateTime.UtcNow;
            }

            foreach (var entry in eventData.Context.ChangeTracker.Entries())
            {
                if (entry is not { State: EntityState.Modified, Entity: ISoftDelete moddified }) continue;
                moddified.UpdatedAt = DateTime.UtcNow;
            }
            return result;
        }
    }
}
