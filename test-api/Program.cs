using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Text.Json.Serialization;
using test_api.libs;
using test_api.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<testapiContext>(opt => 
{
    //opt.UseNpgsql(builder.Configuration.GetConnectionString("testPg"));
    opt.UseSqlServer(builder.Configuration.GetConnectionString("testSQL"), x => x.UseDateOnlyTimeOnly());
    opt.AddInterceptors(new SoftDeleteInterceptor(), new UseTimeStampsInterceptor());
});

builder.Services.AddControllers().AddJsonOptions(opt =>
{
    opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    opt.JsonSerializerOptions.Converters.Add(new DateOnlyJsonConverter());
});


var rulesCors = "RulesCors";
builder.Services.AddCors(opt =>
{
    opt.AddPolicy(rulesCors, builder =>
    {
        builder
            .AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors(rulesCors);

app.UseAuthorization();

app.MapControllers();

app.Run();
